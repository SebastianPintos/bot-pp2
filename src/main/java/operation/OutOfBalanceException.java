package operation;

public class OutOfBalanceException extends Exception {

	private static final long serialVersionUID = 1L;
	
	public OutOfBalanceException(String message) {
		super(message);
	}

}
