package operation;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import market.Market;
import wallet.Wallet;

public class OrderDispatcher implements PropertyChangeListener {
	private final Wallet wallet;
	private final Market market;

	public OrderDispatcher(OrderCreator order, Market market, Wallet wallet) {
		order.addPropertyChangeListener(this);
		this.wallet = wallet;
		this.market = market;
	}

	public void dispatchOrder(Order order) {
		String orderCode = market.placeOrder(order);
		if (orderCode.equals("")) {
			if (order.getType() == OperationType.BUY) {
				unlockMoney(order.getQuantity() * order.getPrice());
			}
			return;
		}
		executeOrder(order);
	}

	private boolean executeOrder(Order order) {
		OperationType type = order.getType();
		double money = order.getQuantity() * order.getPrice();
		try{
			if (type == OperationType.BUY) {
				wallet.withdrawMoney(money);
				unlockMoney(money);
				return true;
			}
			wallet.addMoney(money);
			return true;
		}
		catch(Exception e){ return false;}
	}

	private void unlockMoney(double money) {
		wallet.unlockMoney(money);
	}

	@Override
	public void propertyChange(PropertyChangeEvent event) {
		Order order = (Order) event.getNewValue();
		dispatchOrder(order);
	}
}
