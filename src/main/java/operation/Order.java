package operation;

import java.util.Date;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import cryptoCurrency.CryptoCurrency;

public class Order {
	private double price;
	private Date date;
	private double quantity;
	private CryptoCurrency cryptoCurrency;
	private OperationType type;

	public Order(double price, Date date, double quantity, CryptoCurrency cryptoCurrency, OperationType type) {
		super();
		this.price = price;
		this.date = date;
		this.quantity = quantity;
		this.cryptoCurrency = cryptoCurrency;
		this.type = type;
	}

	public double getPrice() {
		return price;
	}

	public Date getDate() {
		return date;
	}

	public double getQuantity() {
		return quantity;
	}

	public CryptoCurrency getCryptoCurrency() {
		return cryptoCurrency;
	}

	public OperationType getType() {
		return type;
	}
	
	@Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }
 
    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }

}
