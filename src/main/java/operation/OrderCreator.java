package operation;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Date;

import rules.Rule;
import wallet.Wallet;

public class OrderCreator {
	private Wallet wallet;
	private PropertyChangeSupport changes = new PropertyChangeSupport(this);

	public OrderCreator(Wallet wallet) {
		this.wallet = wallet;
	}

	public Order create(double price, Rule rule, Date date) throws OutOfBalanceException {
		double orderAmount = rule.getQuantity() * price;
		if (!walletHasSufficientMoney(rule.getOperationType(), orderAmount)) {
			throw new OutOfBalanceException("Out of Balance");
		}
		if (rule.getOperationType() == OperationType.BUY) {
			wallet.lockMoney(orderAmount);
		}
		Order order = new Order(price, date, rule.getQuantity(), rule.getCryptoCurrency(), rule.getOperationType());
		changes.firePropertyChange("orderCreated", null, order);
		return order;
	}

	private boolean walletHasSufficientMoney(OperationType type, double orderAmount) {
		if (type == OperationType.SELL) {
			return true;
		}
		return wallet.getAvailableMoney() >= orderAmount;
	}

	public void addPropertyChangeListener(PropertyChangeListener l) {
		changes.addPropertyChangeListener(l);
	}
}
