package bot;

import market.Market;
import operation.OrderCreator;
import operation.OutOfBalanceException;
import rules.Rule;
import rules.RuleValidator;

import java.util.Date;
import java.util.List;

public class Bot implements Runnable {
	private List<Rule> rules;
	private Market market;
	private OrderCreator orderCreator;
	private int loopTime;
	private Thread thread;
	private boolean run;

	public Bot(List<Rule> rules, Market market, OrderCreator orderCreator, int loopTime) {
		this.rules = rules;
		this.market = market;
		this.loopTime = loopTime * 60;
		this.orderCreator = orderCreator;
		this.run = true;
		this.thread = new Thread(this, this.getClass().getName());
		thread.start();
	}

	public void operate() {
		for (Rule rule : rules) {
			double marketPrice = market.getPrice(rule.getSymbol());
			if (marketPrice == -1)
				continue;
			if (RuleValidator.validateRule(rule, marketPrice))
				try {
					orderCreator.create(marketPrice, rule, new Date());
				} catch (OutOfBalanceException e) {
				}
		}
	}

	public void setRules(List<Rule> rules) {
		this.rules = rules;
	}

	public Market getMarket() {
		return market;
	}

	public void setMarket(Market market) {
		this.market = market;
	}

	public String getMarketName() {
		return this.market.getName();
	}

	@Override
	public void run() {
		try {
			while (run) {
				if (market.getName() == null)
					continue;
				this.operate();
				Thread.sleep(loopTime);
			}
		} catch (InterruptedException e) {
		}
	}

	public void stopBot() {
		this.run = false;
	}

	public void resume() {
		this.run = true;
	}
}
