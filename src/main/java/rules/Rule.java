package rules;

import operation.OperationType;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import cryptoCurrency.CryptoCurrency;

public class Rule {
	private double deltaPercentage;
	private CryptoCurrency cryptoCurrency;
	private double quantity;
	private OperationType operationType;
	private double referencePrice;

	public Rule(double deltaPercentage, CryptoCurrency cryptoCurrency, double quantity, OperationType operationType,
			double referencePrice) {
		super();
		this.deltaPercentage = deltaPercentage;
		this.cryptoCurrency = cryptoCurrency;
		this.quantity = quantity;
		this.operationType = operationType;
		this.referencePrice = referencePrice;
	}

	public double getDeltaPercentage() {
		return deltaPercentage;
	}

	public double getReferencePrice() {
		return referencePrice;
	}

	public CryptoCurrency getCryptoCurrency() {
		return new CryptoCurrency(this.cryptoCurrency.getName(), this.cryptoCurrency.getSymbol());
	}

	public String getSymbol() {
		return cryptoCurrency.getSymbol();
	}

	public OperationType getOperationType() {
		return this.operationType;
	}

	public double getQuantity() {
		return quantity;
	}

	public boolean isBuy() {
		return getOperationType() == OperationType.BUY;
	}

	public void setReferencePrice(double price) {
		if (price < 0)
			throw new IllegalArgumentException();
		this.referencePrice = price;
	}

	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}

}
