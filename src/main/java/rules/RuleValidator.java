package rules;

public class RuleValidator {

	public static boolean validateRule(Rule rule, double marketPrice) {
		boolean isBuy = rule.isBuy();
		double referencePrice = rule.getReferencePrice();
		if ((isBuy && referencePrice < marketPrice) || (!isBuy && marketPrice < referencePrice)) {
			return false;
		}

		double diff = isBuy ? referencePrice - marketPrice : marketPrice - referencePrice;

		double percentage = Math.abs((diff / referencePrice) * 100);
		return percentage >= rule.getDeltaPercentage();
	}
}
