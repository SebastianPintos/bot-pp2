package market;
import operation.Order;

public interface Market{
	double getPrice(String symbol);
	String getName();
	String placeOrder(Order order);
}
