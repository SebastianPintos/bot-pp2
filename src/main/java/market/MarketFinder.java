package market;

import java.io.File;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.*;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

public class MarketFinder {
	private List<String> marketNames;
	private String path;

	public MarketFinder(String path) {
		this.marketNames = new ArrayList<>();
		this.path = path;
		path = resolveAbsolutePath(path);
	}

	public Market findName(String marketName) {
		Market market = new NullMarket();
		try {
			File file = getFirstFile();
			JarFile jarFile = new JarFile(file);
			Enumeration<JarEntry> entries = jarFile.entries();
			URLClassLoader clazzLoader = URLClassLoader.newInstance(new URL[] { file.toURI().toURL() });

			while (entries.hasMoreElements()) {
				JarEntry element = entries.nextElement();
				jarFile.close();
				if(!isMarket(clazzLoader, element)) continue;
				Class<?> cls = Class.forName(getClassName(element), true, clazzLoader);
				return (Market) cls.getDeclaredConstructor().newInstance();
			}
		    jarFile.close();
		} catch (Exception e) {
		}
		return market;
	}
	
	private File getFirstFile() {
		File file = new File(path);
		File[] files = file.listFiles();
		return files[0];
	}
	
	public List<Market> findMarkets() {
		List<Market> markets = new ArrayList<Market>();
		try {
			File file = new File(path);
			File[] files = file.listFiles();
			if (files.length == 0)
				return markets;
			file = files[0];
			JarFile jarFile = new JarFile(file);

			Enumeration<JarEntry> entries = jarFile.entries();
			URLClassLoader clazzLoader = URLClassLoader.newInstance(new URL[] { file.toURI().toURL() });


			while (entries.hasMoreElements()) {
				JarEntry element = entries.nextElement();
				if(!isMarket(clazzLoader, element)) continue;
				Class<?> cls = Class.forName(getClassName(element), true, clazzLoader);
				markets.add((Market) cls.getDeclaredConstructor().newInstance());
			}
			jarFile.close();

		} catch (Exception e) {
		}
		return markets;
	}
	
	private String getClassName(JarEntry element) {
		return element.getName().replace("/", ".").replace(".class", "");
	}
	
	private boolean isMarket(URLClassLoader classLoader, JarEntry element) {
		try {
			if (!element.getName().endsWith(".class")) return false;		
			String name = getClassName(element);
			Class<?> cls;
			cls = Class.forName(name, true, classLoader);
			if (!Market.class.isAssignableFrom(cls))
				return false;
		} catch (ClassNotFoundException e) {
			return false;
		}
		return true;
	}


	public List<String> findNames() {
		try {
			File file = new File(path);
			for (File f : file.listFiles()) {
				JarFile jarFile = new JarFile(f);
				Enumeration<JarEntry> entries = jarFile.entries();
				URLClassLoader clazzLoader = URLClassLoader.newInstance(new URL[] { f.toURI().toURL() });
				while (entries.hasMoreElements()) {
					JarEntry element = entries.nextElement();
					if(!isMarket(clazzLoader, element)) continue;
					String name = getClassName(element);
					marketNames.add(name.substring(name.lastIndexOf(".") + 1));
				}
				jarFile.close();
			}
		} catch (Exception e) {
		}
		return marketNames;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public List<String> getMarketNames() {
		return marketNames;
	}

	private String resolveAbsolutePath(String path) {
		try {
			return new File("").getAbsolutePath().toString() + File.separator + path;
		} catch (Exception e) {
			return "";
		}
	}
}
