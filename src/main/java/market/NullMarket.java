package market;

import operation.Order;

public class NullMarket implements Market {

	@Override
	public double getPrice(String symbol) {
		return -1;
	}

	@Override
	public String getName() {
		return null;
	}

	@Override
	public String placeOrder(Order order) {
		return null;
	}

}
