package criteriosDeAceptacion;

import static org.junit.Assert.assertEquals;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import bot.Bot;
import cryptoCurrency.CryptoCurrency;
import market.Market;
import mock.MarketFake;
import operation.OperationType;
import operation.OrderCreator;
import operation.OrderDispatcher;
import rules.Rule;
import wallet.Wallet;

public class Story1 {
	private Bot bot;
	private CryptoCurrency asset;
	private List<Rule> rules;
	private Wallet wallet;
	private Rule buyRule1;
	private Rule buyRule2;
	private Rule sellRule1;

	@Mock
	Market market;
	
	@Before
	public void init() {
		MockitoAnnotations.openMocks(this);
		this.asset = new CryptoCurrency("BITCOIN","BTC");
		rules = new ArrayList<>();
		buyRule1 = createRule(15.0, OperationType.BUY, 10.0);
		buyRule2 = createRule(20.0, OperationType.BUY, 10.0);
		sellRule1 = createRule(15.0, OperationType.SELL, 10.0);
		rules.add(buyRule1);
		this.createBot();
	}
	
	@Test
	public void CA1() {
		removeRule(0);
		bot.operate();
		assertEquals(100.0, wallet.getAvailableMoney(), 0);
	}
	
	@Test
	public void CA2() {
		setMarketPrice(8.5);
		bot.operate();
		assertEquals(91.5, wallet.getAvailableMoney(), 0);
	}
	
	@Test
	public void CA3() {
		setMarketPrice(10);
		bot.operate();
		assertEquals(100.0, wallet.getAvailableMoney(), 0);
	}
	
	@Test
	public void CA4() {
		rules.add(sellRule1);
		setMarketPrice(11.5);
		bot.operate();
		assertEquals(111.5, wallet.getAvailableMoney(), 0);
	}

	@Test
	public void CA5() {
		rules.add(buyRule2);
		setMarketPrice(8.0);
		bot.operate();
		assertEquals(84.0, wallet.getAvailableMoney(), 0);
	}
	
	@Test
	public void CA6() {
		setCodeOrder("");
		rules.add(buyRule2);
		setMarketPrice(8.0);
		bot.operate();
		assertEquals(100.0, wallet.getAvailableMoney(), 0);
	}
	
	private void createBot() {
		wallet = new Wallet(100.0);
		Market marketFake = new MarketFake();
		OrderCreator orderCreator = new OrderCreator(wallet);
		@SuppressWarnings("unused")
		OrderDispatcher dispatcher = new OrderDispatcher(orderCreator, marketFake, wallet);
		bot = new Bot(rules, marketFake, orderCreator, 0);
		bot.stopBot();
	}
	
	private Rule createRule(double delta, OperationType type, double referencePrice) {
		return new Rule(delta, asset,1, type, referencePrice);
	}
	
	private void removeRule(int index) {
		rules.remove(index);
	}
	
	private void setMarketPrice(double price) {
		MarketFake fake = (MarketFake) bot.getMarket();
		fake.setPrice(price);
	}
	
	private void setCodeOrder(String code) {
		MarketFake fake = (MarketFake) bot.getMarket();
		fake.setCodeOrder(code);
	}
}
