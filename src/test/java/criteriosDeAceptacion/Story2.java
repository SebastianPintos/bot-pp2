package criteriosDeAceptacion;

import static org.junit.Assert.assertEquals;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import market.Market;
import market.MarketFinder;

public class Story2 {
	private MarketFinder marketFinder;
	
	@Before
	public void init() {
		marketFinder = new MarketFinder("src/test/4/resources/testPlugins/CA1");
	}

	@Test
	public void testIncorrectPathImplementions() throws Exception {
		assertEquals(findMarkets("").size(), 0);
	}
	
	@Test
	public void testFindOneMarketImplemention() throws Exception {
		List<Market> markets = findMarkets("src/test/resources/testPlugins/CA3");
		assertEquals("RIPPIO", markets.get(0).getName());
	}

	@Test
	public void testFindTwoMarketImplementions() {
		List<Market> markets = findMarkets("src/test/resources/testPlugins/CA4");
		assertEquals("RIPPIO", markets.get(0).getName());
		assertEquals("BINANCE", markets.get(1).getName());
	}

	@Test
	public void testFindNotImplementMarket() {
		assertEquals(findMarkets("src/test/resources/testPlugins/CA2").size(), 0);
	}
	
	private List<Market> findMarkets(String path) {
		marketFinder.setPath(path);
		return marketFinder.findMarkets();
	}
}
