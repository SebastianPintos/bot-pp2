package testUnitarios;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;

import cryptoCurrency.CryptoCurrency;
import operation.OperationType;
import operation.Order;
import operation.OrderCreator;
import operation.OutOfBalanceException;
import rules.Rule;
import wallet.Wallet;

public class OrderCreatorTest {
	private OrderCreator orderCreator;
	private CryptoCurrency cryptoCurrency;
	private Wallet wallet;
	private Rule buyRule;
	
	@Before
	public void init() {
		wallet = new Wallet(100);
		orderCreator = new OrderCreator(wallet);
		cryptoCurrency = new CryptoCurrency("BITCOIN","BTC");
		buyRule = new Rule(1, cryptoCurrency, 1, OperationType.BUY, 100);
	}
	
	@Test
	public void testOrderCreatorCreateOk() throws OutOfBalanceException {
		Date date = new Date();
		Order order = new Order(5, date, buyRule.getQuantity(), buyRule.getCryptoCurrency(), buyRule.getOperationType());
		assertEquals(orderCreator.create(5, buyRule, date), order);
	}
	
	@Test (expected = OutOfBalanceException.class)
	public void testOrderCreatorCreateFalse() throws OutOfBalanceException {
		assertNotNull(null, orderCreator.create(110, buyRule, new Date()));
	}
}
