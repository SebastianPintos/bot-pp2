package testUnitarios;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import bot.Bot;
import market.Market;
import mock.MarketFake;
import mock.MarketFake2;
import operation.OrderCreator;
import operation.OrderDispatcher;
import rules.Rule;
import wallet.Wallet;

public class BotTest {
	private Bot bot;
	private List<Rule> rules;
	private Wallet wallet;

	@Mock
	Market market;
	
	@Before
	public void init() {
		MockitoAnnotations.openMocks(this);
	rules = new ArrayList<>();
		this.createBot();
	}
	
	@Test
	public void testBotGetMarketOk() {
		bot.setMarket(new MarketFake2());
		assertEquals(bot.getMarket().getName(), "MARKET2");
	}
	
	@Test
	public void tesBotGetMarketNameOk() {
		bot.setMarket(new MarketFake2());
		assertEquals(bot.getMarketName(), "MARKET2");
	}

	private void createBot() {
		wallet = new Wallet(100.0);
		Market marketFake = new MarketFake();
		OrderCreator orderCreator = new OrderCreator(wallet);
		@SuppressWarnings("unused")
		OrderDispatcher dispatcher = new OrderDispatcher(orderCreator, marketFake, wallet);
		bot = new Bot(rules, marketFake, orderCreator, 0);
		bot.stopBot();
	}
}
