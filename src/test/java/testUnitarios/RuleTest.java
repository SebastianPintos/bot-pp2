package testUnitarios;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import cryptoCurrency.CryptoCurrency;
import operation.OperationType;
import rules.Rule;
import rules.RuleValidator;

public class RuleTest {
	private Rule buyRule;
	private Rule sellRule;
	private CryptoCurrency cryptoCurrency;

	@Before
	public void init() {
		cryptoCurrency = new CryptoCurrency("BITCOIN","BTC");
		buyRule = new Rule(1, cryptoCurrency, 1, OperationType.BUY, 100);
		sellRule = new Rule(1, cryptoCurrency, 1, OperationType.SELL, 100);
	}
	
	@Test
	public void testGetQuantity() {
		assertEquals(1, buyRule.getQuantity(), 0);
	}
	
	@Test
	public void testGetReferencePrice() {
		assertEquals(100, buyRule.getReferencePrice(), 0);
	}
	
	@Test
	public void testGetOperationType() {
		assertEquals(OperationType.BUY, buyRule.getOperationType());
	}
	
	@Test
	public void testGetDeltaPercentage() {
		assertEquals(1, buyRule.getDeltaPercentage(), 1);
	}
	
	@Test
	public void testValidateBuyRuleFail() {
		assertFalse(RuleValidator.validateRule(buyRule, 100));
	}
	
	@Test
	public void testValidateBuyRuleOk() {
		assertTrue(RuleValidator.validateRule(buyRule, 99));
	}
	
	@Test
	public void testValidateSellRuleFail() {
		assertFalse(RuleValidator.validateRule(sellRule, 99));
	}
	
	@Test
	public void testValidateSellRuleOk() {
		assertTrue(RuleValidator.validateRule(sellRule, 101));
	}
	
	@Test
	public void testRuleHashCodeOk() {
		assertNotNull(sellRule.hashCode());
	}
	
	@Test
	public void testRuleEqualsOk() {
		assertTrue(sellRule.equals(sellRule));
	}
	
	@Test
	public void testRuleEqualsFail() {
		assertFalse(sellRule.equals(buyRule));
	}
	
	@Test
	public void testRuleSetReferencePriceOk() {
		sellRule.setReferencePrice(2);
		assertEquals(sellRule.getReferencePrice(), 2.0, 0);
	}
}
