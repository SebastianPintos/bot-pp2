package testUnitarios;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;

import cryptoCurrency.CryptoCurrency;
import operation.OperationType;
import operation.Order;
import operation.OutOfBalanceException;

public class OrderTest {
	private Order order;
	private Date date;
	private CryptoCurrency cryptoCurrency;
	
	@Before
	public void init() {
		date = new Date();
		cryptoCurrency = new CryptoCurrency("BITCOIN", "BTC");
		order = new Order(1, date, 1, cryptoCurrency, OperationType.BUY);
	}
	
	@Test
	public void testOrderHashCodeOk() throws OutOfBalanceException {
		assertNotNull(order.hashCode());
	}
	
	@Test
	public void testOrderGetDateOk() throws OutOfBalanceException {
		assertEquals(order.getDate(), date);
	}
	
	@Test
	public void testOrderGetCryptoCurrencyOk() throws OutOfBalanceException {
		assertEquals(order.getCryptoCurrency(), cryptoCurrency);
	}
}
