package testUnitarios;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import cryptoCurrency.CryptoCurrency;

public class CryptoCurrencyTest {
	private CryptoCurrency cryptoCurrency;

	@Before
	public void init() {
		cryptoCurrency = new CryptoCurrency("BITCOIN","BTC");
	}
	
	@Test
	public void testGetName() {
		assertEquals(cryptoCurrency.getName(), "BITCOIN");
	}
	
	@Test
	public void testGetSymbol() {
		assertEquals(cryptoCurrency.getSymbol(), "BTC");
	}
	
	@Test
	public void testEqualsOk() {
		CryptoCurrency other = new CryptoCurrency(cryptoCurrency.getName(), cryptoCurrency.getSymbol());
		assertEquals(other, cryptoCurrency);
	}
	
	@Test
	public void testEqualsFail() {
		CryptoCurrency other = new CryptoCurrency("NAME", "SYMBOL");
		assertNotEquals(other, cryptoCurrency);
	}
	
	@Test
	public void testHashOk() {
		assertTrue(cryptoCurrency.hashCode() > 0);
	}
}
