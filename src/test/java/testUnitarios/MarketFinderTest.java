package testUnitarios;
import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import market.MarketFinder;

public class MarketFinderTest {
	private MarketFinder marketFinder;

	@Before
	public void init() {
		marketFinder = new MarketFinder("src/test/resources/testPlugins/CA4");
	}
	
	@Test
	public void testMarketGetNameOk() {
		marketFinder.findNames();
		assertEquals("Rippio", marketFinder.findNames().get(0));
	}
	
	@Test
	public void testMarketGetNameFalse() {
		marketFinder.setPath("/SOMEPATH");
		assertEquals(marketFinder.findNames().size(), 0);
	}
}
