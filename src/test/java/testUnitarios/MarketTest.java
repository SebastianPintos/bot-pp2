package testUnitarios;

import static org.junit.Assert.assertEquals;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;

import cryptoCurrency.CryptoCurrency;
import mock.MarketFake;
import operation.OperationType;
import operation.Order;

public class MarketTest {
	private MarketFake market;
	private CryptoCurrency cryptoCurrency;

	@Before
	public void init() {
		market = new MarketFake();
		cryptoCurrency = new CryptoCurrency("BITCOIN", "BTC");
	}
	
	@Test
	public void testMarketGetPrice() {
		market.setPrice(10);
		assertEquals(10, market.getPrice("BTC"), 0);
	}
	
	@Test
	public void testMarketGetName() {
		assertEquals("BINANCE", market.getName());
	}
	
	@Test
	public void testMarketPlaceOrder() {
		Order order = new Order(1.0, new Date(), 1.0, cryptoCurrency, OperationType.BUY);
		assertEquals(market.placeOrder(order), "ABC");
	}
}
