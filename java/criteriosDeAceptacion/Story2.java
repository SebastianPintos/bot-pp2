package criteriosDeAceptacion;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;

import org.junit.Before;
import org.junit.Test;

import market.MarketFinder;

public class Story2 {
	private static String _pathNoImplement = "testPlugin" + File.separator + "NoImplementation";
	private static String _pathOneImplement = "testPlugin" + File.separator + "OneImplementation";
	private static String _pathTwoImplement = "testPlugin" + File.separator + "TwoImplementation";

	private static String _packageNoImplement = "testPlugin.NoImplementation";
	private static String _packageOneImplement = "testPlugin.OneImplementation";
	private static String _packageTwoImplement = "testPlugin.TwoImplementation";
	
	private MarketFinder marketFinder;
	
	@Before
	public void init() {
		marketFinder = new MarketFinder();
	}
  
	@Test //chequeo que retorna si path no contiene archivos 
	public void testIncorrectPathImplementions() throws MalformedURLException  {
		marketFinder.initialize("D:/test", "");
		assertTrue(marketFinder.getMarkets().size()==0);
	}
	
	@Test //chequeo si clase implementa IMarket
		public void testFindNotImplementMarket() throws MalformedURLException  {
		marketFinder.initialize(resolveAbsolutePath(_pathNoImplement), _packageNoImplement);
		assertTrue(marketFinder.getMarkets().size()==0);
	}
	
	@Test //chequeo que retorna se cargo un market implementation
	public void testFindOneMarketImplemention() throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException, IllegalArgumentException, SecurityException, IOException  {
		marketFinder.initialize(resolveAbsolutePath(_pathOneImplement), _packageOneImplement);
		assertTrue(marketFinder.getMarkets().size()==1);
	}

	@Test //chequeo que retorna se cargo mas de un market implementation 
	public void testFindTwoMarketImplementions() throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException, IllegalArgumentException, SecurityException, IOException  {
		marketFinder.initialize(resolveAbsolutePath(_pathTwoImplement), _packageTwoImplement);
		assertTrue(marketFinder.getMarkets().size()==2);
	}
	
	private static String resolveAbsolutePath(String packageName) {
		try {
			return new File("").getAbsolutePath().toString() + File.separator + packageName ;
		}
		catch (Exception e) {			
			return "";
		}		
	}
}
