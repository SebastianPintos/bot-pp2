package testUnitarios;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import market.MarketFinder;

public class MarketFinderTest {
	private static String _pathOneImplement = "testPlugin" + File.separator + "OneImplementation";
	private static String _packageOneImplement = "testPlugin.OneImplementation";
	private MarketFinder marketFinder;
	
	@Before
	public void init() {
		marketFinder = new MarketFinder();
	}
	
	@Test
	public void testFindOneMarketImplemention() throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException, IllegalArgumentException, SecurityException, IOException  {
		marketFinder.initialize(resolveAbsolutePath(_pathOneImplement), _packageOneImplement);
		assertTrue(marketFinder.getMarkets().size()==1);
		assertTrue(marketFinder.getMarket("CRYPTOMARKET")!=null);
		List<String> names = new ArrayList<String>();
		names.add("CRYPTOMARKET");
		assertTrue(marketFinder.getNames().equals(names));
	}
	
	private static String resolveAbsolutePath(String packageName) {
		try {
			return new File("").getAbsolutePath().toString() + File.separator + packageName ;
		}
		catch (Exception e) {			
			return "";
		}		
	}
}
