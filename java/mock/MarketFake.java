package mock;

import market.Market;
import operation.Order;

public class MarketFake implements Market{
	private double price = 100;
	private String codeOrder = null;

	@Override
	public double getPrice(String symbol) {
		return price;
	}

	@Override
	public String getName() {
		return "BINANCE";
	}

	@Override
	public String placeOrder(Order order) {
		return codeOrder;
	}

	public void setPrice(double price) {
		this.price = price;
	}
	
	public void setCodeOrder(String codeOrder) {
		this.codeOrder = codeOrder;
	}
}
